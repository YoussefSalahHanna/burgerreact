import React from 'react';
import './BuildControls.css';
import BuildControl from './BuildControl/BuildControl';
const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' }
];
const BuildControls = (props) => (
    <div className='Controls'>
        <p>Current Price: <strong>{props.price.toFixed(2)}</strong></p>
        {controls.map(ctrl => (
            <BuildControl
                key={ctrl.label}
                type={ctrl.type}
                label={ctrl.label}
                disabled={props.disabled[ctrl.type]}
                added={() => props.more(ctrl.type)}
                removed={() => props.less(ctrl.type)}
            />
        ))}
        <button disabled={!props.purchasable} className='OrderButton'>ORDER NOW</button>
    </div>
);
export default BuildControls;