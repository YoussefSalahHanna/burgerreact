import React from 'react';
import BurgerIngredients from './BurgerIngredients/BurgerIngredients';
import './Burger.css';

const Burger = (props) => {
    let TrIngredients = Object.keys(props.ingredint)
    .map(igKey => {
         return [...Array(props.ingredint[igKey])].map( (_, i) => {
            return <BurgerIngredients key={igKey + i}  type={igKey}/>;
        });
    })
    .reduce((arr, el) =>{
        return arr.concat(el);
    }, []); 
    if (TrIngredients.length === 0) {
        TrIngredients = <p>Please start adding ingredints!</p>
    }
    return (
        <div className='Burger'>
            <BurgerIngredients type='bread-top'/>
            {TrIngredients}
            <BurgerIngredients type='bread-bottom'/>
        </div>
    )
}
export default Burger;