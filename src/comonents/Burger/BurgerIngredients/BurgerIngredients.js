import React, { Component } from 'react';
import './BurgerIngredients.css';
import PropTypes from 'prop-types'

class BurgerIngredients extends Component {
    render() {
        let ingredint = null;
        switch (this.props.type) {
            case ('bread-bottom'):
                ingredint = <div className='BreadBottom'></div>
                break;
            case ('bread-top'):
                ingredint = (
                    <div className='BreadTop'>
                        <div className='Seeds1'></div>
                        <div className='Seeds2'></div>
                    </div>
                );
                break;
            case ('meat'):
                ingredint = <div className='Meat'></div>
                break;
            case ('cheese'):
                ingredint = <div className='Cheese'></div>
                break;
            case ('bacon'):
                ingredint = <div className='Bacon'></div>
                break;
            case ('salad'):
                ingredint = <div className='Salad'></div>
                break;
            default:
                ingredint = null;
        }
        return ingredint;
    }
}
BurgerIngredients.propTypes = {
    type: PropTypes.string.isRequired
}
export default BurgerIngredients;