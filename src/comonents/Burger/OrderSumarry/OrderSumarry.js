import React from 'react';
import Aux from '../../../hoc/Auxl';
const OrderSumarry = (props) => {
    const sumarry = Object.keys(props.ingredint).map(
        el => {
            return <li key={el} >
                <span style={
                    { textTransform: 'capitalize' }} > {el}: </span>{props.ingredint[el]} </li>
        }
    );
    return (
        <Aux>
            <h3> Your Order </h3>
            <p> A delicious burger with the following ingredients: </p> {sumarry}
            <p> Continue to checkout ? </p>
        </Aux>
    );
}

export default OrderSumarry;