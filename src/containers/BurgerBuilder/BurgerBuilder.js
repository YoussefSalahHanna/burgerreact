import React, { Component } from 'react';
import Aux from '../../hoc/Auxl';
import Burger from '../../comonents/Burger/Burger';
import BuildControls from '../../comonents/Burger/BuildControls/BuildControls';
import Modal from '../../comonents/UI/Modal/Modal';
import OrderSumarry from '../../comonents/Burger/OrderSumarry/OrderSumarry';
const INGREDIENT_PRICES = {
    salad: .5,
    cheese: .4,
    meat: 1.3,
    bacon: .7
}
class BurgerBuilder extends Component {
    state = {
        ingredint: {
            salad: 0,
            bacon: 0,
            cheese: 0,
            meat: 0
        },
        totalPrice: 4,
        purchasable: false
    }
    updatePurchasableState = (ingredints) => {
        const sum = Object.keys(ingredints)
            .map(igKey => {
                return ingredints[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);
        this.setState({ purchasable: sum > 0 })
    }
    addIngredintHandler = (type) => {
        const oldCount = this.state.ingredint[type];
        const updatedCount = oldCount + 1;
        const updatedIngredint = {
            ...this.state.ingredint
        }
        updatedIngredint[type] = updatedCount;
        const price = INGREDIENT_PRICES[type] + this.state.totalPrice
        this.setState({ totalPrice: price, ingredint: updatedIngredint })
        this.updatePurchasableState(updatedIngredint);
    }
    removeIngredintHandler = (type) => {
        const oldCount = this.state.ingredint[type];
        if (oldCount < 1) {
            return;
        }
        const updatedCount = oldCount - 1;
        const updatedIngredint = {
            ...this.state.ingredint
        }
        updatedIngredint[type] = updatedCount;
        const price = this.state.totalPrice - INGREDIENT_PRICES[type]
        this.setState({ totalPrice: price, ingredint: updatedIngredint })
        this.updatePurchasableState(updatedIngredint);
    }
    render() {
        const disabled = { ...this.state.ingredint };
        for (let key in disabled) {
            disabled[key] = disabled[key] <= 0
        }
        return (
            <Aux>
                <Modal>
                    <OrderSumarry ingredint={this.state.ingredint} />
                </Modal>
                <Burger ingredint={this.state.ingredint} />
                <BuildControls
                    disabled={disabled}
                    price={this.state.totalPrice}
                    purchasable={this.state.purchasable}
                    more={this.addIngredintHandler}
                    less={this.removeIngredintHandler} />
            </Aux>
        )
    }
}
export default BurgerBuilder;